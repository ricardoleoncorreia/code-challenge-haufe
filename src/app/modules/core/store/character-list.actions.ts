import { Action } from '@ngrx/store';

export const StoreAction = {
  INCREMENT_PAGE: '[Character List Component] Increment Page',
  DECREMENT_PAGE: '[Character List Component] Decrement Page',
  UPDATE_SEARCH_NAME: '[Character List Component] Update Search Name',
};

export class IncrementPageAction implements Action {
  readonly type = StoreAction.INCREMENT_PAGE;

  constructor(public payload: number) {}
}

export class DecrementPageAction implements Action {
  readonly type = StoreAction.DECREMENT_PAGE;
}

export class UpdateSearchNameAction implements Action {
  readonly type = StoreAction.UPDATE_SEARCH_NAME;

  constructor(public payload: string) {}
}
