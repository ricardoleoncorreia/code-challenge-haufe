import { Action } from '@ngrx/store';
import {
  IncrementPageAction,
  UpdateSearchNameAction,
  StoreAction,
} from './character-list.actions';

export interface CharacterListState {
  currentPage: number;
  currentSearchName: string;
}

export const initialState: CharacterListState = {
  currentPage: 1,
  currentSearchName: '',
};

const createNewState = (
  state: CharacterListState,
  data: Partial<CharacterListState>
) => {
  return { ...state, ...data };
};

export function characterListReducer(
  state: CharacterListState = initialState,
  action: Action
) {
  switch (action.type) {
    case StoreAction.INCREMENT_PAGE:
      const totalPages = (action as IncrementPageAction).payload;
      if (state.currentPage >= totalPages) return state;
      return createNewState(state, { currentPage: state.currentPage + 1 });
    case StoreAction.DECREMENT_PAGE:
      if (state.currentPage <= 1) return state;
      return createNewState(state, { currentPage: state.currentPage - 1 });
    case StoreAction.UPDATE_SEARCH_NAME:
      const currentSearchName = (action as UpdateSearchNameAction).payload;
      return createNewState(state, { currentSearchName });
    default:
      return state;
  }
}
