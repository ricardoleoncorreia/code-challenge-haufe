export const currentPageSelector = (state: any) =>
  state.characterList.currentPage;
export const currentSearchNameSelector = (state: any) =>
  state.characterList.currentSearchName;
export const fullStateSelector = (state: any) => state.characterList;
