import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable, shareReplay } from 'rxjs';
import {
  DecrementPageAction,
  IncrementPageAction,
} from '../../core/store/character-list.actions';
import { CharacterListState } from '../../core/store/character-list.reducer';
import { currentPageSelector } from '../../core/store/character-list.selector';

@Component({
  selector: 'app-paginator',
  templateUrl: 'paginator.component.html',
  styleUrls: ['paginator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginatorComponent {
  @Input() totalPages = 5;

  private readonly firstPage = 1;
  currentPage$: Observable<number>;

  get disablePreviousClass$(): Observable<any> {
    return this.currentPage$.pipe(
      map((page) => ({ 'paginator--disabled': page <= this.firstPage }))
    );
  }

  get disableNextClass$(): Observable<any> {
    return this.currentPage$.pipe(
      map((page) => ({ 'paginator--disabled': page >= this.totalPages }))
    );
  }

  constructor(private readonly store: Store<CharacterListState>) {
    this.currentPage$ = this.store
      .select(currentPageSelector)
      .pipe(shareReplay());
  }

  previousPage(): void {
    this.store.dispatch(new DecrementPageAction());
  }

  nextPage(): void {
    this.store.dispatch(new IncrementPageAction(this.totalPages));
  }
}
