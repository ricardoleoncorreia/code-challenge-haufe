import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SearchBoxComponent } from './search-box.component';

@NgModule({
  declarations: [SearchBoxComponent],
  imports: [CommonModule, FormsModule],
  exports: [SearchBoxComponent],
})
export class SearchBoxModule {}
