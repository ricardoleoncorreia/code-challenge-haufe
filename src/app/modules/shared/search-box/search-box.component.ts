import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { map, Observable, Subject, takeUntil, tap } from 'rxjs';
import { Store } from '@ngrx/store';
import { CharacterListState } from '../../core/store/character-list.reducer';
import { currentSearchNameSelector } from '../../core/store/character-list.selector';
import { UpdateSearchNameAction } from '../../core/store/character-list.actions';

@Component({
  selector: 'app-search-box',
  templateUrl: 'search-box.component.html',
  styleUrls: ['search-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchBoxComponent implements OnDestroy {
  searchText: string;
  currentSearchName$: Observable<boolean>;

  private destroy$ = new Subject<void>();

  constructor(private readonly store: Store<CharacterListState>) {
    this.store
      .select(currentSearchNameSelector)
      .pipe(
        takeUntil(this.destroy$),
        tap((searchName) => (this.searchText = searchName))
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onInputChange(): void {
    this.store.dispatch(new UpdateSearchNameAction(this.searchText));
  }
}
