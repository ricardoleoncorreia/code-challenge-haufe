import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { CharacterList } from './character.namespace';
import { capitalize } from '../shared/utils/capitalize';

type SearchParams = CharacterList.SearchParams;
type CharacterListData = CharacterList.Data;
type CharacterData = CharacterList.Result;

@Injectable({ providedIn: 'root' })
export class CharacterService {
  constructor(private http: HttpClient) {}

  getCharacterListData(
    searchParams: SearchParams
  ): Observable<CharacterListData> {
    const params = this.paramsBuilder(searchParams);

    return this.http.get<CharacterListData>('character', { params }).pipe(
      map((list) => {
        list.results = list.results.map((result) => {
          result.origin.name = capitalize(result.origin.name);
          result.status = capitalize(result.status);
          return result;
        });
        return list;
      })
    );
  }

  getCharacterData(characterId: number): Observable<CharacterData> {
    return this.http.get<CharacterData>(`character/${characterId}`);
  }

  private paramsBuilder(searchParams: SearchParams): HttpParams | undefined {
    const props = Object.keys(searchParams) as CharacterList.SearchParamsProp[];

    if (props.length === 0) return undefined;
    return props.reduce(
      (params, prop) => params.append(prop, searchParams[prop] || ''),
      new HttpParams()
    );
  }
}
