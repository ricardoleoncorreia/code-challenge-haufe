import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { CharacterList } from '../character.namespace';

@Component({
  selector: 'app-character-summary-card',
  templateUrl: 'character-summary-card.component.html',
  styleUrls: ['character-summary-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CharacterSummaryCardComponent implements OnInit {
  @Input() character: CharacterList.Result;

  routerLink: string[];

  ngOnInit(): void {
    this.routerLink = [`/character/${this.character.id}`];
  }
}
