import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, EMPTY, map, Observable } from 'rxjs';
import { capitalize } from '../../shared/utils/capitalize';
import { CharacterService } from '../character.service';

interface RouteParams {
  characterId: string;
}

interface CharacterViewModel {
  image: string;
  details: { title: string; content: string }[];
}

@Component({
  selector: 'app-character-details',
  templateUrl: 'character-details.component.html',
  styleUrls: ['character-details.component.scss'],
})
export class CharacterDetailsComponent implements OnInit {
  characterData$: Observable<CharacterViewModel>;
  isError: boolean;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly characterService: CharacterService
  ) {}

  ngOnInit(): void {
    const routeParams = this.activatedRoute.snapshot.params as RouteParams;
    this.characterData$ = this.characterService
      .getCharacterData(+routeParams.characterId)
      .pipe(
        map((data) => ({
          image: data.image,
          details: [
            { title: 'Name', content: data.name },
            { title: 'Gender', content: data.gender },
            { title: 'Status', content: data.status },
            { title: 'Species', content: data.species },
            { title: 'Last KnownLocation', content: data.location.name },
            { title: 'Origin', content: capitalize(data.origin.name) },
          ],
        })),
        catchError(() => {
          this.isError = true;
          return EMPTY;
        })
      );
  }

  goToList(): void {
    this.router.navigate(['/character']);
  }
}
