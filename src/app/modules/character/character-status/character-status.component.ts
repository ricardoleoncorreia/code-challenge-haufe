import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CharacterList } from '../character.namespace';

enum CharaterStatus {
  alive = 'alive',
  dead = 'dead',
  unknown = 'unknown',
}

interface CharacterStatusIcon {
  'status--alive': boolean;
  'status--dead': boolean;
  'status--unknown': boolean;
}

@Component({
  selector: 'app-character-status',
  templateUrl: 'character-status.component.html',
  styleUrls: ['character-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CharacterStatusComponent {
  @Input() character: CharacterList.Result;

  private get isAlive(): boolean {
    return this.character.status.toLowerCase() === CharaterStatus.alive;
  }

  private get isDead(): boolean {
    return this.character.status.toLowerCase() === CharaterStatus.dead;
  }

  private get isUnknown(): boolean {
    return this.character.status.toLowerCase() === CharaterStatus.unknown;
  }

  get statusIconStyle(): CharacterStatusIcon {
    return {
      'status--alive': this.isAlive,
      'status--dead': this.isDead,
      'status--unknown': this.isUnknown,
    };
  }
}
