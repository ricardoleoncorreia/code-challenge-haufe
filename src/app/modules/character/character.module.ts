import { NgModule } from '@angular/core';

import { CharacterListComponent } from './character-list/character-list.component';
import { CharacterDetailsComponent } from './character-details/character-details.component';
import { CharacterRoutingModule } from './character-routing.module';
import { CommonModule } from '@angular/common';
import { CharacterSummaryCardComponent } from './character-summary-card/character-summary-card.component';
import { SearchBoxModule } from '../shared/search-box/search-box.module';
import { PaginatorModule } from '../shared/paginator/paginator.module';
import { LoaderModule } from '../shared/loader/loader.module';
import { CharacterStatusComponent } from './character-status/character-status.component';

@NgModule({
  declarations: [
    CharacterListComponent,
    CharacterDetailsComponent,
    CharacterSummaryCardComponent,
    CharacterStatusComponent,
  ],
  imports: [
    CommonModule,
    CharacterRoutingModule,
    SearchBoxModule,
    PaginatorModule,
    LoaderModule,
  ],
})
export class CharacterModule {}
