import { Component } from '@angular/core';
import { map, Observable, switchMap, tap } from 'rxjs';
import { Store } from '@ngrx/store';
import { CharacterListState } from '../../core/store/character-list.reducer';
import { CharacterList } from '../character.namespace';
import { CharacterService } from '../character.service';
import { fullStateSelector } from '../../core/store/character-list.selector';

@Component({
  selector: 'app-character-list',
  templateUrl: 'character-list.component.html',
  styleUrls: ['character-list.component.scss'],
})
export class CharacterListComponent {
  characterList$: Observable<CharacterList.Result[]>;
  characterInfo: CharacterList.Info = {} as CharacterList.Info;
  isLoading: boolean = true;

  constructor(
    private readonly store: Store<CharacterListState>,
    private readonly characterService: CharacterService
  ) {
    this.characterList$ = this.store.select(fullStateSelector).pipe(
      tap(() => (this.isLoading = true)),
      switchMap((state) =>
        this.characterService.getCharacterListData({
          name: state.currentSearchName,
          page: state.currentPage,
        })
      ),
      tap((data) => {
        this.isLoading = false;
        this.characterInfo = data.info;
      }),
      map((data) => data.results)
    );
  }
}
