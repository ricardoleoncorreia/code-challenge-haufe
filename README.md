# Code Challenge Haufe

This project was created to solve a Technical Assesment. The minimum requirements are:

1. List of character’s view:

   - This should be the main page of the application, it should contain cards with at least the picture and name of the characters.
   - The user should be able to search characters by name.
   - The list should be paginated.

2. Character details view:

   - When an element of the list is clicked, you’ll navigate to the detail view.
   - The user should be able to go back to the list using the UI.
   - The user should be able to refresh the page and still see the character information.
   - Feel free to develop the detail page with as much information from the API as you want, but it should at least show the name, status, species and origin.

3. 404 page.

## Run the application locally

- Clone the [repo](https://gitlab.com/ricardoleoncorreia/code-challenge-haufe).
- Install the dependencies using `npm install`.
- Start the development server with `npm start`.

## Live Demo

Check a live demo on https://code-challenge-haufe.ricardoleoncorreia.com.
